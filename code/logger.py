import logging

class Logger:

    _user=None
    _dbName=None

    @staticmethod
    def setUp(user, dbName):
        Logger._user=user
        Logger._dbName=dbName
        logging.basicConfig(filename='journal.log', level=logging.INFO,
        format='%(asctime)-15s %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
    
    @staticmethod
    def setUpDbName(dbName):
        Logger._dbName=dbName

    @staticmethod
    def logShort(msg):
        msgf=' {group}:{login} {action}'
        logging.info(msgf.format(group=Logger._user.getGroup(),
                    login=Logger._user.getLogin(),
                    action=msg))


    @staticmethod
    def logBeforeInit(msg):
        msgf = ' Before init: {action}'
        logging.info(msgf.format(action=msg))


    @staticmethod
    def log(msg):
        msgf=' {group}:{login} ({db}) {action} '
        logging.info(msgf.format(group=Logger._user.getGroup(),
                    login=Logger._user.getLogin(),
                    db=Logger._dbName,
                    action=msg))