from tkinter import *
from tkinter import ttk

from .table import Table
from .view import View


class TableView(View):
    def __init__(self, root, vm, vmType, font, headers, colWidths, hasDeleteBt):
        self._idUpdField = None
        self._idField = None
        self._deleteWidgetsSet = None
        self._updateWidgetsSet = None
        self._updateIdWidgetsSet = None
        self._addWidgetsSet = None
        self._typeOpVar = None
        self._addOpt = 0
        self._updateOpt = 1
        self._deleteOpt = 2
        self._updateIdOpt = 3

        self._subFrmRight = None
        self._table = None
        self._subFrmLeft = None
        self._fioFrm = None
        self._headers = headers
        self._colWidths = colWidths
        self._hasDeleteBt = hasDeleteBt
        super().__init__(root=root, vm=vm, vmType=vmType, font=font)

    def pack(self, *vars, **kwars):
        super().pack(*vars, **kwars)
        self._table.setData(self._vm.getTableData())

    def createView(self):
        self._table = Table(self, self._headers, self._vm.getTableData(), self._font, self._colWidths)
        self._configureStyles()
        self.addDefaultWidgets()

    def addDefaultWidgets(self):
        self._subFrmLeft = ttk.Frame(self)
        self._subFrmRight = ttk.Frame(self, padding=10)
        self.addTypeOpt()
        self.addIdFields()
        self.addButtons()

        self._subFrmLeft.grid(row=1, column=0, sticky='w')
        self._subFrmRight.grid(row=1, column=1, sticky='w')

    def addWidget(self, widget, AUState: 'Tuple of enable states for Add Update actions'):
        if AUState[0]: self._addWidgetsSet.add(widget)
        if AUState[1]: self._updateWidgetsSet.add(widget)
        self._typeOpVar.set(self._typeOpVar.get())

    def getWigetsFrame(self):
        return self._subFrmLeft

    def getFioFrame(self):
        if not self._fioFrm:
            self._fioFrm = ttk.Frame(self.getWigetsFrame())
            self._fioFrm.pack(anchor='nw')
        return self._fioFrm

    def evalFunc(self):
        if self._typeOpVar.get() == self._addOpt:
            self._vm.addCommand()
        elif self._typeOpVar.get() == self._updateOpt:
            self._vm.editCommand()
        elif self._typeOpVar.get() == self._deleteOpt:
            self._vm.deleteCommand()
        elif self._typeOpVar.get() == self._updateIdOpt:
            self._vm.updateIdCommand()
        self._table.setData(self._vm.getTableData())

    def addButtons(self):
        evalBt = ttk.Button(self._subFrmRight, text='Выполнить', style=self._btStyleName, command=self.evalFunc)
        evalBt.grid(row=0)

    def _typeOpChangeEventHandler(self, var, index, mode):
        allWidgets = set(list(self._addWidgetsSet) + list(self._updateWidgetsSet) + list(self._deleteWidgetsSet) + list(
            self._updateIdWidgetsSet))
        variables = ((self._addOpt, self._addWidgetsSet),
                     (self._updateOpt, self._updateWidgetsSet),
                     (self._deleteOpt, self._deleteWidgetsSet),
                     (self._updateIdOpt, self._updateIdWidgetsSet),)
        for varType, wList in variables:
            if self._typeOpVar.get() == varType:
                for i in wList:
                    i.pack(anchor='nw')
                for i in allWidgets - wList:
                    i.pack_forget()

    def addTypeOpt(self):

        # select operation type
        self._typeOpVar = IntVar(value=self._updateOpt)
        typeOpFrm = ttk.Frame(self._subFrmLeft)
        typeOpLb = ttk.Label(typeOpFrm, text='Тип операции:', font=self._font)
        updateRb = ttk.Radiobutton(typeOpFrm, text='Изменить', value=self._updateOpt, variable=self._typeOpVar,
                                   style=self._rbStyleName)
        updateIdRb = ttk.Radiobutton(typeOpFrm, text='Изменить ID', value=self._updateIdOpt, variable=self._typeOpVar,
                                     style=self._rbStyleName)
        addRb = ttk.Radiobutton(typeOpFrm, text='Добавить', value=self._addOpt, variable=self._typeOpVar,
                                style=self._rbStyleName)
        typeOpLb.grid(row=0, column=0)
        updateRb.grid(row=0, column=2)
        updateIdRb.grid(row=0, column=3)
        addRb.grid(row=0, column=1)
        typeOpFrm.pack(anchor='nw')

        if self._hasDeleteBt:
            deleteRb = ttk.Radiobutton(typeOpFrm, text='Удалить', value=self._deleteOpt, variable=self._typeOpVar,
                                       style=self._rbStyleName)
            deleteRb.grid(row=0, column=4)

        # add type change event handler
        self._addWidgetsSet = set()
        self._updateWidgetsSet = set()
        self._updateIdWidgetsSet = set()
        self._deleteWidgetsSet = set()

        self._typeOpVar.trace_add('write', self._typeOpChangeEventHandler)

    def _configureStyles(self):
        self._rbStyleName = 'tvFrm.TRadiobutton'
        rbStyle = ttk.Style()
        rbStyle.configure(self._rbStyleName, font=self._font)

        self._btStyleName = 'tvFrm.TButton'
        btStyle = ttk.Style()
        btStyle.configure(self._btStyleName, font=self._font, padding=5)

    def addIdFields(self):
        # select id
        self._idField = View.addField(self._subFrmLeft, font=self._font, text='ID', var=self._vm.idVar, width=8,
                                      check_errs=True, validator=self._vm.isValidUID,
                                      invalid_msg='Идентификатор должен быть представлен в таблице')
        self._updateWidgetsSet.add(self._idField)
        self._deleteWidgetsSet.add(self._idField)
        self._updateIdWidgetsSet.add(self._idField)
        # modify id
        self._idUpdField = View.addField(self._subFrmLeft, font=self._font, text='новый ID', var=self._vm.idUpdVar,
                                         width=8,
                                         check_errs=True, validator=self._vm.isValidUniqueUID,
                                         invalid_msg='Идентификатор не должен быть представлен в таблице')
        self._updateIdWidgetsSet.add(self._idUpdField)
