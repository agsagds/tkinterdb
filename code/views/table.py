from tkinter import *
from tkinter import ttk


class Table:
    width = 1000 - 60
    height = 200

    def __init__(self, root, headers, data, font, widths):
        self.root = root
        self.data = data

        self.cellStyle = Table.getCellStyle()
        self.font = font
        self.widths = widths
        self.headers = headers
        self._createTable()

    @staticmethod
    def getCellStyle():
        tableStyleName = "Table.TLabel"
        tableStyle = ttk.Style()
        tableStyle.configure(tableStyleName, foreground="black", background="white", borderwidth=2, relief='groove')
        return tableStyleName

    def _createTable(self):
        topFrm = ttk.Frame(self.root, padding=10)
        # https://blog.teclado.com/tkinter-scrollable-frames/
        tCnv = Canvas(topFrm, width=Table.width, height=Table.height)
        tScroll = ttk.Scrollbar(topFrm, orient="vertical", command=tCnv.yview)
        self.tFrm = ttk.Frame(tCnv)
        self.tFrm.bind("<Configure>", lambda _: tCnv.configure(scrollregion=tCnv.bbox("all")))
        tCnv.create_window((0, 0), window=self.tFrm, anchor='nw')
        tCnv.config(yscrollcommand=tScroll.set)

        self._createHeaders()
        self._createCells()

        tScroll.grid(row=0, column=1, sticky='ns')
        tCnv.grid(row=0, column=0, sticky="news")
        topFrm.grid(row=0, columnspan=2)

    def _createHeaders(self):
        self.headersFrm = ttk.Frame(self.tFrm)
        font = list(self.font)
        if len(font) < 3:
            font.append('bold')
        else:
            font[3] = 'bold'
        for i in range(len(self.headers)):
            ttk.Label(self.headersFrm,
                      text=self.headers[i],
                      style=self.cellStyle,
                      font=font,
                      width=self.widths[i]).grid(row=0, column=i)
        self.headersFrm.grid(row=0)

    def _createCells(self):
        self.contentFrm = ttk.Frame(self.tFrm)
        self.cells = [0] * len(self.data)
        for i in range(len(self.data)):
            self.cells[i] = [0] * len(self.data[i])
            for j in range(len(self.data[i])):
                self.cells[i][j] = ttk.Label(self.contentFrm, text=self.data[i][j].get(), style=self.cellStyle,
                                             font=self.font, width=self.widths[j])
                self.cells[i][j].grid(row=i, column=j)
        self.contentFrm.grid(row=1)

    def deleteRowById(self, id):
        for i in range(len(self.getData())):
            if self.data[i][0] == id:
                for j in range(len(self.data[i])):
                    self.cells[i][j].destroy()
                del (self.cells[i])
                del (self.data[i])
                break

    def addRow(self, rowData):
        self.data.append(rowData)
        self.cells.append([0] * len(rowData))
        for j in range(len(rowData)):
            self.cells[-1][j] = ttk.Label(self.contentFrm, text=rowData[j], style=self.cellStyle, font=self.font,
                                          width=self.widths[j])
            self.cells[-1][j].grid(row=len(self.cells) - 1, column=j, sticky='news')
        self.contentFrm.update_idletasks()

    def getRow(self, row):
        if (row >= len(self.cells) or row < 0): return []
        return self.cells[row][:]

    def rows(self):
        return len(self.cells)

    def getData(self):
        return self.data

    def clear(self):
        for i in range(len(self.cells)):
            for j in range(len(self.cells[i])):
                self.cells[i][j].destroy()
        del (self.cells[:])
        del (self.data[:])

    def setData(self, data):
        self.clear()
        self.data = data
        self._createCells()
