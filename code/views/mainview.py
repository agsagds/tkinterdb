from tkinter import *
from tkinter import ttk, filedialog, messagebox
from ..viewModels.validators import Validator as V
from ..models.dbmanager import DBManager
from ..models.user import User
from ..logger import Logger
from ..viewModels.vmfactory import VMFactory


class MainView(ttk.Frame):
    def __init__(self, root, vmfactory, vfactoryType):
        super().__init__(root)
        self._vmfactory = vmfactory
        self._vfactoryType = vfactoryType
        self._font = self._vfactoryType.defaultFont
        self._configureStyles()
        self.createView()
        self._createLoginView()

    def createView(self):
        self._addFrames()
        self._addFrameSelection()
        self._addSaveLoadButtons()

    def _addFrames(self):
        self._viewFrm = ttk.Frame(self)
        self._views = []
        self._views.append((self._vfactoryType.getAddressesView(self._viewFrm, self._vmfactory), 'Адреса'))
        self._views.append((self._vfactoryType.getDoctorsView(self._viewFrm, self._vmfactory), 'Доктора'))
        self._views.append((self._vfactoryType.getPatientsView(self._viewFrm, self._vmfactory), 'Пациенты'))
        self._views.append((self._vfactoryType.getEmergencyView(self._viewFrm, self._vmfactory), 'Экстренные контакты'))
        self._views.append((self._vfactoryType.getConsultationsView(self._viewFrm, self._vmfactory), 'Консультации'))

        self._viewFrm.grid(row=0)

    def destroyView(self):
        for i in self._views:
            i[0].destroy()
        del (self._views)
        self._viewFrm.destroy()
        self._viewSelFrm.destroy()
        self._viewBtFrm.destroy()

    def loadCommand(self):
        filename = filedialog.askopenfilename(defaultextension='.dat', filetypes=[('Данные', '*.dat')])
        if filename == '': return
        db = DBManager.load(filename)
        self.destroyView()
        del self._vmfactory
        self._vmfactory = VMFactory(db)
        Logger.setUpDbName(self._vmfactory.getDB().getName())
        Logger.logShort(f'Загрузка базы данных {self._vmfactory.getDB().getName()}')
        self.createView()

    def saveCommand(self):
        filename = filedialog.asksaveasfilename(defaultextension='.dat', filetypes=[('Данные', '*.dat')])
        if filename == '': return
        DBManager.save(self._vmfactory.getDB(), filename)
        Logger.logShort(f'База данных сохранена как {self._vmfactory.getDB().getName()}')
        Logger.setUpDbName(self._vmfactory.getDB().getName())

    def loginCommand(self):
        if not User.isUser(self._loginVar.get(), self._passVar.get()):
            messagebox.showerror(title='Отказано в доступе', message='Неверный логин и/или пароль')
            return
        Logger.setUp(User(self._loginVar.get(), self._passVar.get()), self._vmfactory.getDB().getName())
        Logger.logShort('Вход')
        self._showMainFrm()

    def _configureStyles(self):
        self._rbStyleName = 'mFrm.TRadiobutton'
        rbStyle = ttk.Style()
        rbStyle.configure(self._rbStyleName, font=self._font)

        self._btStyleName = 'mFrm.TButton'
        btStyle = ttk.Style()
        btStyle.configure(self._btStyleName, font=self._font, padding=5)

    def _activeFrameChangeEventHandler(self, var, index, mode):
        for i in range(len(self._views)):
            if self._viewNumVar.get() == i:
                self._views[i][0].pack(anchor='nw')
            else:
                self._views[i][0].pack_forget()

    def _addFrameSelection(self):
        # select operation type
        self._viewNumVar = IntVar()
        self._viewSelFrm = ttk.Frame(self)

        for i in range(len(self._views)):
            rb = ttk.Radiobutton(self._viewSelFrm, text=self._views[i][1], value=i, variable=self._viewNumVar,
                                 style=self._rbStyleName)
            rb.grid(row=0, column=i)

        self._viewSelFrm.grid(row=1)
        self._viewNumVar.trace_add('write', self._activeFrameChangeEventHandler)
        self._viewNumVar.set(0)

    def _addSaveLoadButtons(self):
        self._viewBtFrm = ttk.Frame(self)
        saveBt = ttk.Button(self._viewBtFrm, text='Сохранить', style=self._btStyleName, command=self.saveCommand)
        saveBt.grid(row=0, column=0)
        loadBt = ttk.Button(self._viewBtFrm, text='Загрузить', style=self._btStyleName, command=self.loadCommand)
        loadBt.grid(row=0, column=1)

        self._viewBtFrm.grid(row=2)

    def _createLoginView(self):
        self._loginVar = StringVar()
        self._passVar = StringVar()
        self._loginFrm = ttk.Frame(self)
        loginLb = ttk.Label(self._loginFrm, text='Логин', font=self._font, width=10)
        loginEnt = ttk.Entry(self._loginFrm, textvariable=self._loginVar, width=12)

        passLb = ttk.Label(self._loginFrm, text='Пароль', font=self._font, width=10)
        passEnt = ttk.Entry(self._loginFrm, textvariable=self._passVar, show='*', width=12)

        loginBt = ttk.Button(self._loginFrm, text='Войти', style=self._btStyleName, command=self.loginCommand)

        loginLb.grid(row=0, column=0, sticky='w')
        loginEnt.grid(row=0, column=1, sticky='w')
        passLb.grid(row=1, column=0, sticky='w')
        passEnt.grid(row=1, column=1, sticky='w')
        loginBt.grid(row=2, column=0, columnspan=2, sticky='w')

        self._hideMainFrm()

        self._loginFrm.grid(row=0)

    def _hideMainFrm(self):
        self._viewFrm.grid_forget()
        self._viewSelFrm.grid_forget()
        self._viewBtFrm.grid_forget()

    def _showMainFrm(self):
        self._loginFrm.grid_forget()
        self._viewFrm.grid(row=0)
        self._viewSelFrm.grid(row=1)
        self._viewBtFrm.grid(row=2)
