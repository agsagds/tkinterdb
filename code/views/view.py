from tkinter import *
from tkinter import ttk
from .utils import decorator
from ..viewModels.validators import Validator as V


class View(ttk.Frame):
    def __init__(self, root, vm, vmType, font=()):
        super().__init__(root)
        self._vm = vm
        self._vmType = vmType
        self._font = font
        self.createView()

    def createView(self):
        pass

    '''
        addField(root[, [text],[font],[var],[width],[row],[column],
            [err_label],[check_errs],[validator],[validator_params],[invalid_msg])
            
        text - текст надписи перед полем
        font - параметры шрифта
        var - переменная, асоциированная с полем для ввода
        width - ширина поля для ввода
        label_width - ширина надписи
        row - номер начальной строки в контейнере
        column - номер начального столбца в контейнере
        err_label - надпись, асоциированная с сообщениями об ошибках
        check_errs - (True, False) флаг валидации ошибок
        validator - функция-валидатор
        invalid_msg - сообщение об ошибке
    '''
    @staticmethod
    def addField(root, **kwars):
        if 'text' not in kwars: kwars['text'] = ''
        if 'font' not in kwars: kwars['font'] = ('Times New Roman', 13)
        if 'var' not in kwars: kwars['var'] = None
        if 'width' not in kwars: kwars['width'] = 20
        if 'label_width' not in kwars: kwars['label_width'] = len(kwars['text'])

        fieldFrm = ttk.Frame(root)
        lb = ttk.Label(fieldFrm, text=kwars['text'], font=kwars['font'], width=kwars['label_width'])
        ent = ttk.Entry(fieldFrm, textvariable=kwars['var'], width=kwars['width'])

        lb.grid(row=0, column=0, sticky='w')
        ent.grid(row=0, column=1, sticky='w')

        if 'row' not in kwars and 'column' not in kwars:
            fieldFrm.pack(anchor='nw')
        else:
            if 'row' not in kwars: kwars['row'] = 0
            if 'column' not in kwars: kwars['column'] = 0
            fieldFrm.grid(row=kwars['row'], column=kwars['column'], sticky='w')

        if 'check_errs' in kwars and kwars['check_errs']:

            if 'err_label' not in kwars:
                errLb = ttk.Label(fieldFrm, text='', font=kwars['font'], foreground='red', wraplength='300')
                errLb.grid(row=1, column=0, columnspan=2, sticky='w')
                kwars['err_label'] = errLb

            if 'invalid_msg' not in kwars: kwars['invalid_msg'] = 'error'
            checkerHandler = root.register(decorator(V.validate, kwars['err_label'], ent, kwars['validator']))
            invalidHandler = root.register(decorator(V.onInvalid, kwars['err_label'], ent, kwars['invalid_msg']))
            ent.config(validate='focusout', validatecommand=checkerHandler, invalidcommand=invalidHandler)

        return fieldFrm

    @staticmethod
    def addSelectField(root, **kwars):
        if 'text' not in kwars: kwars['text'] = ''
        if 'font' not in kwars: kwars['font'] = ('Times New Roman', 13)
        if 'var' not in kwars: kwars['var'] = None
        if 'width' not in kwars: kwars['width'] = 20
        if 'label_width' not in kwars: kwars['label_width'] = len(kwars['text'])
        if 'values' not in kwars: kwars['values'] = ('default')

        fieldFrm = ttk.Frame(root)
        lb = ttk.Label(fieldFrm, text=kwars['text'], font=kwars['font'], width=kwars['label_width'])
        ent = ttk.Combobox(fieldFrm, values=kwars['values'], textvariable=kwars['var'], width=kwars['width'])
        ent.current(0)
        ent.state(['readonly'])

        lb.grid(row=0, column=0, sticky='w')
        ent.grid(row=0, column=1, sticky='w')

        return fieldFrm
