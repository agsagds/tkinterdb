from .mainview import MainView
from ..viewModels.addressesvm import AddressesVM
from ..viewModels.doctorsvm import DoctorsVM
from ..viewModels.patientsvm import PatientsVM
from ..viewModels.consultationsvm import ConsultationsVM
from ..viewModels.emcontactsvm import EmContactsVM
from .tableview import TableView
from .view import View


class ViewFactory:
    _mainView = None
    defaultFont = ('Arial', 13)

    @staticmethod
    def getMainWindow(root, vmfactory):
        if ViewFactory._mainView is None:
            ViewFactory._mainView = MainView(root, vmfactory, ViewFactory)
        return ViewFactory._mainView

    @staticmethod
    def getAddressesView(root, vmfactory):
        vm = vmfactory.getAddressesVM()
        view = TableView(root,
                         vm=vm,
                         vmType=AddressesVM,
                         font=ViewFactory.defaultFont,
                         headers=('ID', 'Город', 'Улица', 'Дом', 'Пациент'),
                         colWidths=(5, 10, 65, 10, 10),
                         hasDeleteBt=True)

        city = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Город', var=vm.cityVar,
                             width=10, label_width=8,
                             check_errs=True, validator=vm.isValidCity,
                             invalid_msg='Допускаюстя только буквы. Первая в верхнем регистре - остальные в нижнем')
        view.addWidget(city, (1, 1))
        street = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Улица', var=vm.streetVar,
                               width=20, label_width=8,
                               check_errs=True, validator=vm.isValidStreet,
                               invalid_msg='Значение не может быть пустым')
        view.addWidget(street, (1, 1))
        home = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Дом', var=vm.homeVar, width=3,
                             label_width=8,
                             check_errs=True, validator=vm.isValidHome,
                             invalid_msg='Номер дома может быть только положительным числом')
        view.addWidget(home, (1, 1))
        homePart = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Корпус', var=vm.homePartVar,
                                 width=3, label_width=8,
                                 check_errs=True, validator=vm.isValidHomePart,
                                 invalid_msg='Корпус может быть только пустым или односимвольным буквоцифровым значением')
        view.addWidget(homePart, (1, 1))
        patient = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Пациент', var=vm.patientVar,
                                width=3, label_width=8,
                                check_errs=True, validator=vm.isValidPatient,
                                invalid_msg='Пациент с таким ID должен существовать и не должна уже существовать ассоциированная с ним запись')
        view.addWidget(patient, (1, 1))
        return view

    @staticmethod
    def getDoctorsView(root, vmfactory):
        vm = vmfactory.getDoctorsVM()
        view = TableView(root,
                         vm=vm,
                         vmType=DoctorsVM,
                         font=ViewFactory.defaultFont,
                         headers=('ID', 'ФИО', 'Телефон', 'Пол', 'Кабинет', 'Специализация'),
                         colWidths=(5, 25, 12, 5, 10, 15),
                         hasDeleteBt=True)

        secondName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Фамилия', var=vm.snameVar,
                                   width=25,
                                   label_width=8,
                                   check_errs=True, validator=vm.isValidSecondName,
                                   invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        firstName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Имя', var=vm.fnameVar,
                                  width=25,
                                  label_width=8,
                                  check_errs=True, validator=vm.isValidFirstName,
                                  invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        lastName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Отчество', var=vm.lnameVar,
                                 width=25,
                                 label_width=8,
                                 check_errs=True, validator=vm.isValidLastName,
                                 invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        view.addWidget(secondName, (1, 1))
        view.addWidget(firstName, (1, 1))
        view.addWidget(lastName, (1, 1))
        phone = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Телефон', var=vm.phoneVar,
                              width=15, label_width=8,
                              check_errs=True, validator=vm.isValidPhone,
                              invalid_msg='Некорректный формат номера телефона. Используйте +X(XXX)XXX-XX-XX')
        view.addWidget(phone, (1, 1))
        sex = View.addSelectField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Пол', var=vm.sexVar,
                                  width=3, label_width=8,
                                  values=('М', 'Ж'))
        view.addWidget(sex, (1, 0))
        cab = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Кабинет', var=vm.cabinetVar,
                            width=5, label_width=8,
                            check_errs=True, validator=vm.isValidCabinet,
                            invalid_msg='Кабинет описывается натуральным числом')
        view.addWidget(cab, (1, 1))
        spec = View.addSelectField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Специализация',
                                   var=vm.specVar, width=10, label_width=8,
                                   values=('Хирург', 'Отоларинголог', 'Окулист', 'Гинеколог', 'Дерматолог', 'Терапевт',
                                           'Педиатр', 'Психолог'))
        view.addWidget(spec, (1, 0))
        return view

    @staticmethod
    def getPatientsView(root, vmfactory):
        vm = vmfactory.getPatientsVM()
        view = TableView(root,
                         vm=vm,
                         vmType=PatientsVM,
                         font=ViewFactory.defaultFont,
                         headers=('ID', 'ФИО', 'Телефон', 'Пол', 'Экстр. контакт'),
                         colWidths=(5, 25, 15, 5, 20),
                         hasDeleteBt=True)

        secondName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Фамилия', var=vm.snameVar,
                                   width=25,
                                   label_width=8,
                                   check_errs=True, validator=vm.isValidSecondName,
                                   invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        firstName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Имя', var=vm.fnameVar,
                                  width=25,
                                  label_width=8,
                                  check_errs=True, validator=vm.isValidFirstName,
                                  invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        lastName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Отчество', var=vm.lnameVar,
                                 width=25,
                                 label_width=8,
                                 check_errs=True, validator=vm.isValidLastName,
                                 invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        view.addWidget(secondName, (1, 1))
        view.addWidget(firstName, (1, 1))
        view.addWidget(lastName, (1, 1))
        phone = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Телефон', var=vm.phoneVar,
                              width=15, label_width=8,
                              check_errs=True, validator=vm.isValidPhone,
                              invalid_msg='Некорректный формат номера телефона. Используйте +X(XXX)XXX-XX-XX')
        view.addWidget(phone, (1, 1))
        sex = View.addSelectField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Пол', var=vm.sexVar,
                                  width=3, label_width=8,
                                  values=('М', 'Ж'))
        view.addWidget(sex, (1, 0))
        contact = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Экстренный контакт',
                                var=vm.contactVar, width=5, label_width=8,
                                check_errs=True, validator=vm.isValidEmContactId,
                                invalid_msg='Может быть -1 или значением идентификатора из таблицы экстренных контактов')
        view.addWidget(contact, (1, 1))
        return view

    @staticmethod
    def getEmergencyView(root, vmfactory):
        vm = vmfactory.getEmergencyVM()
        view = TableView(root,
                         vm=vm,
                         vmType=EmContactsVM,
                         font=ViewFactory.defaultFont,
                         headers=('ID', 'ФИО', 'Телефон'),
                         colWidths=(5, 50, 30),
                         hasDeleteBt=True)

        secondName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Фамилия', var=vm.snameVar,
                                   width=25,
                                   label_width=8,
                                   check_errs=True, validator=vm.isValidSecondName,
                                   invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        firstName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Имя', var=vm.fnameVar,
                                  width=25,
                                  label_width=8,
                                  check_errs=True, validator=vm.isValidFirstName,
                                  invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        lastName = View.addField(view.getFioFrame(), font=ViewFactory.defaultFont, text='Отчество', var=vm.lnameVar,
                                 width=25,
                                 label_width=8,
                                 check_errs=True, validator=vm.isValidLastName,
                                 invalid_msg='Допускаюстя только буквы. Первая буква в верхнем регистре - остальные в нижнем')
        view.addWidget(secondName, (1, 1))
        view.addWidget(firstName, (1, 1))
        view.addWidget(lastName, (1, 1))
        phone = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Телефон', var=vm.phoneVar,
                              width=15, label_width=8,
                              check_errs=True, validator=vm.isValidPhone,
                              invalid_msg='Некорректный формат номера телефона. Используйте +X(XXX)XXX-XX-XX')
        view.addWidget(phone, (1, 1))
        return view

    @staticmethod
    def getConsultationsView(root, vmfactory):
        vm = vmfactory.getConsultVM()
        view = TableView(root,
                         vm=vm,
                         vmType=ConsultationsVM,
                         font=ViewFactory.defaultFont,
                         headers=('ID', 'Пациент', 'Доктор', 'Описание'),
                         colWidths=(5, 10, 10, 75),
                         hasDeleteBt=True)

        patient = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Пациент', var=vm.patientVar,
                                width=3, label_width=8,
                                check_errs=True, validator=vm.isValidPatientId,
                                invalid_msg='Пациент с таким ID должен существовать в таблице Пациенты')
        view.addWidget(patient, (1, 0))
        doctor = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Врач', var=vm.doctorVar,
                               width=3, label_width=8,
                               check_errs=True, validator=vm.isValidDoctorId,
                               invalid_msg='Врач с таким ID должен существовать в таблице Доктора')
        view.addWidget(doctor, (1, 0))
        reason = View.addField(view.getWigetsFrame(), font=ViewFactory.defaultFont, text='Описание', var=vm.reasonVar,
                               width=40, label_width=8,
                               check_errs=True, validator=vm.isValidReason,
                               invalid_msg='Поле не может быть пустым')
        view.addWidget(reason, (1, 1))
        return view
