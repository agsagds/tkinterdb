def decorator(func, *vals):
    def wrapper(*vars):
        return func(*vals)
    return wrapper


