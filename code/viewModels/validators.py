import re
class Validator:
    @staticmethod
    def isValidNaturalNum(val):
        return val.isdigit() and int(val)>0

    @staticmethod
    def isValidAlnumSymbol(val):
        return len(val)==1 and val.isalnum()
    
    @staticmethod
    def isValidEmpty(val):
        return len(val)==0
    
    @staticmethod
    def isValidNotEmpty(val):
        return len(val)!=0
    
    @staticmethod
    def isValidPhone(val):
        pattern = r'^[\+]?[1-9]?[(]?[0-9]{3}[)]?[0-9]{3}[-\s]?[0-9]{2}[-\s]?[0-9]{2}$'
        return not (re.fullmatch(pattern, val) is None)


    @staticmethod
    def isValidProperName(val):
        return len(val)>0 and val.isalpha() and val[0].isupper() and val[1:].islower()
    
    @staticmethod
    def validate(errLabel, sender, validate_func):
        if not validate_func():
            return False
        errLabel['text']=''
        sender['foreground']='black'
        return True
    
    @staticmethod
    def onInvalid(errLabel, sender, msg):
        errLabel['text'] = msg
        sender['foreground'] = 'red'