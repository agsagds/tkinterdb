from ..models.dbmanager import DBManager
from .addressesvm import AddressesVM
from .doctorsvm import DoctorsVM
from .patientsvm import PatientsVM
from .emcontactsvm import EmContactsVM
from .consultationsvm import ConsultationsVM
from ..logger import Logger


class VMFactory:
    def __init__(self, db):
        self._db = db
        self._mainvm = None
        self._addressvm = None
        self._consultvm = None
        self._doctorsvm = None
        self._patientsvm = None
        self._constactsvm = None

    def getAddressesVM(self):
        if self._addressvm is None:
            self._addressvm = AddressesVM(data=self._db.getAddressTable(),
                                          patientsData=self._db.getPatientsTable())
        return self._addressvm

    def getConsultVM(self):
        if self._consultvm is None:
            self._consultvm = ConsultationsVM(data=self._db.getConsultTable(),
                                              patientsData=self._db.getPatientsTable(),
                                              doctorsData=self._db.getDoctorsTable())
        return self._consultvm

    def getDoctorsVM(self):
        if self._doctorsvm is None:
            self._doctorsvm = DoctorsVM(data=self._db.getDoctorsTable(),
                                        updateIdCallback=self.doctorIdUpd,
                                        deleteCallback=self.doctorDelete)
        return self._doctorsvm

    def getPatientsVM(self):
        if self._patientsvm is None:
            self._patientsvm = PatientsVM(data=self._db.getPatientsTable(),
                                          contactsData=self._db.getEmergencyTable(),
                                          updateIdCallback=self.patientIdUpd,
                                          deleteCallback=self.patientDelete)
        return self._patientsvm

    def getEmergencyVM(self):
        if self._constactsvm is None:
            self._constactsvm = EmContactsVM(data=self._db.getEmergencyTable(),
                                             updateIdCallback=self.emcontactIdUpd,
                                             deleteCallback=self.emcontactDelete)
        return self._constactsvm

    def getDB(self):
        return self._db

    def doctorIdUpd(self, oldId, newId):
        for dat in self.getConsultVM().vmData:
            if dat.doctor.get() == oldId:
                recordStr = str(dat)
                dat.doctor.set(newId)
                Logger.log(f'В таблице {self.getConsultVM()._tableName} обновлена запись [{recordStr}] на [{dat}]')

    def doctorDelete(self, id):
        for_delete = []
        for dat in self.getConsultVM().vmData:
            if dat.doctor.get() == id:
                for_delete.append(dat.id.get())
        for i in for_delete:
            if not self.getConsultVM().deleteRecordById(i):
                return False
        return True

    def patientIdUpd(self, oldId, newId):
        for dat in self.getConsultVM().vmData + self.getAddressesVM().vmData:
            if dat.patient.get() == oldId:
                recordStr = str(dat)
                dat.patient.set(newId)
                if (dat in self.getConsultVM().vmData):
                    Logger.log(f'В таблице {self.getConsultVM()._tableName} обновлена запись [{recordStr}] на [{dat}]')
                else:
                    Logger.log(
                        f'В таблице {self.getAddressesVM()._tableName} обновлена запись [{recordStr}] на [{dat}]')

    def patientDelete(self, id):
        for_delete = []
        for dat in self.getConsultVM().vmData:
            if dat.patient.get() == id:
                for_delete.append(dat.id.get())
        for i in for_delete:
            if not self.getConsultVM().deleteRecordById(i):
                return False

        for_delete = []
        for dat in self.getAddressesVM().vmData:
            if dat.patient.get() == id:
                for_delete.append(dat.id.get())
        for i in for_delete:
            if not self.getAddressesVM().deleteRecordById(i):
                return False

        return True

    def emcontactIdUpd(self, oldId, newId):
        for dat in self.getPatientsVM().vmData:
            if dat.contact.get() == oldId:
                recordStr = str(dat)
                dat.contact.set(newId)
                Logger.log(f'В таблице {self.getPatientsVM()._tableName} обновлена запись [{recordStr}] на [{dat}]')

    def emcontactDelete(self, id):
        for dat in self.getPatientsVM().vmData:
            if dat.contact.get() == id:
                recordStr = str(dat)
                dat.contact.set(-1)
                Logger.log(f'В таблице {self.getPatientsVM()._tableName} обновлена запись [{recordStr}] на [{dat}]')
        return True
