from tkinter import *
from tkinter import ttk, filedialog, messagebox
from .viewmodel import ViewModel
from .validators import Validator as V


class ContactsVM(ViewModel):
    def __init__(self, data, typeModel, typeViewModel, updateIdCallback=lambda *_, **p: None,
                 deleteCallback=lambda *_, **p: True):
        super().__init__(data, typeModel, typeViewModel, updateIdCallback, deleteCallback)
        self.phoneVar = StringVar()
        self.fnameVar = StringVar()
        self.snameVar = StringVar()
        self.lnameVar = StringVar()

    def isValidFirstName(self):
            name_part = self.fnameVar.get()
            return V.isValidProperName(name_part)

    def isValidSecondName(self):
            name_part = self.snameVar.get()
            return V.isValidProperName(name_part)

    def isValidLastName(self):
        name_part = self.lnameVar.get()
        return V.isValidProperName(name_part) or len(name_part) == 0

    def isValidPhone(self):
        return V.isValidPhone(self.phoneVar.get())

    def isValidEdit(self):
        return self.isValidFirstName() and \
               self.isValidSecondName() and \
               self.isValidLastName() and \
               self.isValidPhone()

    def getAddArgs(self):
        return {'name': ' '.join((self.snameVar.get(), self.fnameVar.get(), self.lnameVar.get())),
                'phone': self.phoneVar.get()}

    def updateRecord(self, record):
        super().updateRecord(record)
        record.fname.set(self.fnameVar.get())
        record.sname.set(self.snameVar.get())
        record.lname.set(self.lnameVar.get())
        record.phone.set(self.phoneVar.get())

    def getTableData(self):
        data = super().getTableData()
        for i in range(len(self.vmData)):
            data[i].append(StringVar(value=self.vmData[i].getName())) #TODO: maybe need sep and view too
            data[i].append(self.vmData[i].phone)
        return data
