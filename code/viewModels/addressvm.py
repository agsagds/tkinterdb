from tkinter import *
from .recordvm import RecordVM
class AddressVM(RecordVM):
    def __init__(self, address):
        super().__init__(address)
        
        self.city=StringVar(value=address.getCity())
        self.city.trace_add('write',
            lambda *_: address.setCity(self.city.get()))

        self.street=StringVar(value=address.getStreet())
        self.street.trace_add('write',
            lambda *_: address.setStreet(self.street.get()))
            
        self.home=StringVar(value=address.getHome())
        self.home.trace_add('write',
            lambda *_: address.setHome(self.home.get()))
        
        self.patient=IntVar(value=address.getPatient())
        self.patient.trace_add('write',
            lambda *_: address.setPatient(self.patient.get()))
    
    def __str__(self):
        return super().__str__()+', '+self.city.get()+", '"+self.street.get()+"', "+\
        self.home.get()+', '+str(self.patient.get())