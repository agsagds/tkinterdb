from tkinter import *
from .patientvm import PatientVM
from .personsvm import PersonsVM
from ..models.patient import Patient

class PatientsVM(PersonsVM):
    def __init__(self, data, contactsData, updateIdCallback=lambda *_, **p:None, deleteCallback=lambda *_, **p: True):
        super().__init__(data, Patient, PatientVM, updateIdCallback, deleteCallback)
        self._tableName='Пациенты'
        self._emcontactsData=contactsData
        self.contactVar=IntVar()   
    
    def isValidEmContactId(self):
        return  self.contactVar.get()==-1 or self.contactVar.get() in (i.getId() for i in self._emcontactsData)
    
    def isValidEdit(self):
        return super().isValidEdit() and self.isValidEmContactId()

    def getAddArgs(self):
        d=super().getAddArgs()
        d['idContact']=self.contactVar.get()
        return d
    
    def updateRecord(self, record):
        super().updateRecord(record)
        record.contact.set(self.contactVar.get())

    def getTableData(self):
        data=super().getTableData()
        for i in range(len(self.vmData)):
            data[i].append(self.vmData[i].contact)
        return data