from tkinter import *
from .personvm import PersonVM


class DoctorVM(PersonVM):
    def __init__(self, doctor):
        super().__init__(doctor)

        self.cabinet = IntVar(value=doctor.getCabinet())
        self.cabinet.trace_add('write',
                               lambda *_: doctor.setCabinet(self.cabinet.get()))

        self.speciality = StringVar(value=doctor.getSpeciality())

    def __str__(self):
        return super().__str__() + ', ' + str(self.cabinet.get()) + ', ' + self.speciality.get()
