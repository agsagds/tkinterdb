from tkinter import *
from .recordvm import RecordVM


class ContactVM(RecordVM):
    def __init__(self, contact):
        super().__init__(contact)

        self.phone = StringVar(value=contact.getPhone())
        self.phone.trace_add('write',
                             lambda *_: contact.setPhone(self.phone.get()))

        fio = contact.getName().split()
        print(fio)
        self.fname = StringVar(value=fio[0])
        self.sname = StringVar(value=(fio[1] if len(fio) > 1 else ''))
        self.lname = StringVar(value=(fio[2] if len(fio) > 2 else ''))
        self.fname.trace_add('write',
                             lambda *_: contact.setName(' '.join((self.sname.get(),
                                                                  self.fname.get(),
                                                                  self.lname.get()))))

    def getName(self):
        return ' '.join((self.sname.get(),
                         self.fname.get(),
                         self.lname.get()))

    def __str__(self):
        return super().__str__() + ', ' + self.getName() + ', ' + self.phone.get()
