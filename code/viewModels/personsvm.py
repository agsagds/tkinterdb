from tkinter import *
from .contactsvm import ContactsVM

class PersonsVM(ContactsVM):
    def __init__(self, data, typeModel, typeViewModel, updateIdCallback=lambda *_, **p:None, deleteCallback=lambda *_, **p: True):
        super().__init__(data, typeModel, typeViewModel, updateIdCallback, deleteCallback)
        self.sexVar=StringVar()
    
    def isValidEdit(self):
        return super().isValidEdit()
    
    def getAddArgs(self):
        d=super().getAddArgs()
        d['sex']=self.sexVar.get()
        return d
    
    def updateRecord(self, record):
        super().updateRecord(record)
    
    def getTableData(self):
        data=super().getTableData()
        for i in range(len(self.vmData)):
            data[i].append(self.vmData[i].sex)
        return data