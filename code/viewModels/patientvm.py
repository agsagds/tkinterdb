from tkinter import *
from .personvm import PersonVM
class PatientVM(PersonVM):
    def __init__(self, patient):
        super().__init__(patient)

        self.contact=IntVar(value=patient.getEmergencyContact())
        self.contact.trace_add('write',
            lambda *_: patient.setEmergencyContact(self.contact.get()))
    
    def __str__(self):
        return super().__str__()+', '+str(self.contact.get())