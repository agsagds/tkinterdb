from tkinter import *
from .contactvm import ContactVM
class PersonVM(ContactVM):
    def __init__(self, person):
        super().__init__(person)

        self.sex=StringVar(value=person.getSex())
    
    def __str__(self):
        return super().__str__()+', '+self.sex.get()