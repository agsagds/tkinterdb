from tkinter import *
from .recordvm import RecordVM
class ConsultationVM(RecordVM):
    def __init__(self, consult):
        super().__init__(consult)
        
        self.patient=IntVar(value=consult.getPatient())
        self.patient.trace_add('write',
            lambda *_: consult.setPatient(self.patient.get()))

        self.doctor=IntVar(value=consult.getDoctor())
        self.doctor.trace_add('write',
            lambda *_: consult.setDoctor(self.doctor.get()))
            
        self.reason=StringVar(value=consult.getReason())
        self.reason.trace_add('write',
            lambda *_: consult.setReason(self.reason.get()))
    
    def __str__(self):
        return super().__str__()+', '+str(self.patient.get())+', '+str(self.doctor.get())+", '"+self.reason.get()+"'"