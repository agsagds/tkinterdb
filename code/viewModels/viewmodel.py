from tkinter import *
from tkinter import ttk, filedialog, messagebox
from ..logger import Logger


class ViewModel():
    def __init__(self, data, typeModel, typeViewModel, updateIdCallback=lambda *_, **p: None,
                 deleteCallback=lambda *_, **p: True):
        self._tableName = ''
        self._mType = typeModel
        self._vmType = typeViewModel
        self._mData = data
        self._updateIdCallback = updateIdCallback
        self._deleteCallback = deleteCallback
        self.vmData = [self._vmType(i) for i in data]
        self.idVar = IntVar()
        self.idUpdVar = IntVar()

    def getUID(self):
        return max((i.getId() for i in self._mData)) + 1 if len(self._mData) > 0 else 1

    def isValidUID(self):
        return self.idVar.get() in (i.getId() for i in self._mData)

    def isValidUniqueUID(self):
        return not (self.idUpdVar.get() in (i.getId() for i in self._mData))

    def isValid(self):
        return self.isValidEdit()

    def isValidEdit(self):
        return True

    def getAddArgs(self):
        return dict({})

    def updateRecord(self, record):
        pass

    def updateRecordId(self, record):
        self._updateIdCallback(oldId=record.id.get(), newId=self.idUpdVar.get())
        recordStr = str(record)
        record.id.set(self.idUpdVar.get())
        Logger.log(f'В таблице {self._tableName} обновлена запись [{recordStr}] на [{record}]')

    def getTableData(self):
        data = []
        for i in self.vmData:
            data.append([])
            data[-1].append(i.id)
        return data

    def deleteRecordById(self, id):
        if not self._deleteCallback(id):
            messagebox.showerror(title='Операция не выполнена', message='Ошибка в связанной операции')
            return False
        for i in range(len(self.vmData)):
            if self.vmData[i].id.get() == id:
                self.vmData.pop(i)
                break
        for i in range(len(self._mData)):
            if self._mData[i].getId() == id:
                self._mData.pop(i)
                break
        Logger.log(f'В таблице {self._tableName} удалена запись с ID={id}')
        return True

    def deleteCommand(self):
        if not self.isValidUID():
            messagebox.showerror(title='Операция не выполнена', message='Запись с таким ID отсутствует в базе')
            return False
        return self.deleteRecordById(self.idVar.get())

    def editCommand(self):
        if not self.isValidUID():
            messagebox.showerror(title='Операция не выполнена', message='Запись с таким ID отсутствует в базе')
            return
        if not self.isValidEdit():
            messagebox.showerror(title='Операция не выполнена', message='Неверный формат данных')
            return
        record = None
        for i in self.vmData:
            if i.id.get() == self.idVar.get():
                record = i
                break
        recordStr = str(record)
        self.updateRecord(record)
        Logger.log(f'В таблице {self._tableName} обновлена запись [{recordStr}] на [{record}]')

    def updateIdCommand(self):
        if not self.isValidUID():
            messagebox.showerror(title='Операция не выполнена', message='Запись с таким ID отсутствует в базе')
            return
        if not self.isValidUniqueUID():
            messagebox.showerror(title='Операция не выполнена', message='Неверный формат данных')
            return
        record = None
        for i in self.vmData:
            if i.id.get() == self.idVar.get():
                record = i
                break
        self.updateRecordId(record)

    def addCommand(self):
        if not self.isValid():
            messagebox.showerror(title='Операция не выполнена', message='Неверный формат данных')
            return
        record = self._mType(id=self.getUID(), **self.getAddArgs())
        self._mData.append(record)
        self.vmData.append(self._vmType(record))
        Logger.log(f'В таблицу {self._tableName} добавлена запись [{self.vmData[-1]}]')
