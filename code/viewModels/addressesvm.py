from tkinter import *
from tkinter import ttk, filedialog, messagebox

from code.models import patient
from .viewmodel import ViewModel
from .addressvm import AddressVM
from ..models.address import Address
from .validators import Validator as V

class AddressesVM(ViewModel):
    def __init__(self, data, patientsData, updateIdCallback=lambda *_, **p:None, deleteCallback=lambda *_, **p: True):
        super().__init__(data, Address, AddressVM, updateIdCallback, deleteCallback)
        self._tableName='Адреса'
        self._patients=patientsData
        self.cityVar=StringVar()
        self.streetVar=StringVar()
        self.homeVar=StringVar()  
        self.homePartVar=StringVar()
        self.patientVar=IntVar()       
    

    def isValidHome(self):
        return V.isValidNaturalNum(self.homeVar.get())
    

    def isValidHomePart(self):
        return V.isValidAlnumSymbol(self.homePartVar.get()) or V.isValidEmpty(self.homePartVar.get())
    

    def isValidCity(self):
        return V.isValidProperName(self.cityVar.get())
    

    def isValidStreet(self):
        return V.isValidNotEmpty(self.streetVar.get())
    
    def isValidPatient(self):
        if self.patientVar.get() in (i.patient.get() for i in self.vmData if i.id.get()!=self.idVar.get()):
            return False
        if self.patientVar.get() not in (i.getId() for i in self._patients):
            return False
        return True

    def isValidEdit(self):
        return super().isValidEdit() and \
            self.isValidHome() and \
            self.isValidHomePart() and \
            self.isValidCity() and \
            self.isValidStreet() and \
            self.isValidPatient()

    def getAddArgs(self):
        return {'city':self.cityVar.get(),
            'street':self.streetVar.get(),
            'home':self.homeVar.get()+self.homePartVar.get(),
            'patientId':self.patientVar.get()}
    
    def updateRecord(self, record):
        record.city.set(self.cityVar.get())
        record.street.set(self.streetVar.get())
        record.home.set(self.homeVar.get()+self.homePartVar.get())
        record.patient.set(self.patientVar.get())

    def getTableData(self):
        data=super().getTableData()
        for i in range(len(self.vmData)):
            data[i].append(self.vmData[i].city)
            data[i].append(self.vmData[i].street)
            data[i].append(self.vmData[i].home)
            data[i].append(self.vmData[i].patient)
        return data