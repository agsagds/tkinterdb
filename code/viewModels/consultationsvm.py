from tkinter import *
from tkinter import ttk, filedialog, messagebox
from .viewmodel import ViewModel
from .consultationvm import ConsultationVM
from ..models.consultation import Consultation
from .validators import Validator as V

class ConsultationsVM(ViewModel):
    def __init__(self, data, patientsData, doctorsData, updateIdCallback=lambda *_, **p:None, deleteCallback=lambda *_, **p: True):
        super().__init__(data, Consultation, ConsultationVM, updateIdCallback, deleteCallback)
        self._tableName='Консультации'
        self._patientsData=patientsData
        self._doctorsData=doctorsData
        self.doctorVar=IntVar()
        self.patientVar=IntVar()
        self.reasonVar=StringVar()       
    
    def isValidPatientId(self):
        return self.patientVar.get() in (i.getId() for i in self._patientsData)

    def isValidDoctorId(self):
        return self.doctorVar.get() in (i.getId() for i in self._doctorsData)

    def isValidReason(self):
        return V.isValidNotEmpty(self.reasonVar.get())
    
    def isValidEdit(self):
        return super().isValidEdit() and self.isValidReason()
    
    def getAddArgs(self):
        d=super().getAddArgs()
        d['idPatient']=self.patientVar.get()
        d['idDoctor']=self.doctorVar.get()
        d['reason']=self.reasonVar.get()
        return d
    
    def updateRecord(self, record):
        super().updateRecord(record)
        record.reason.set(self.reasonVar.get())

    def getTableData(self):
        data=super().getTableData()
        for i in range(len(self.vmData)):
            data[i].append(self.vmData[i].patient)
            data[i].append(self.vmData[i].doctor)
            data[i].append(self.vmData[i].reason)
        return data