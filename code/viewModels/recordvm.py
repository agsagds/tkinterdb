from tkinter import *
class RecordVM:
    def __init__(self, record):
        self.id=IntVar(value=record.getId())
        self.id.trace_add('write',
            lambda *_: record.setId(self.id.get()))
    
    def __str__(self):
        return str(self.id.get())