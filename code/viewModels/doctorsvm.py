from tkinter import *
from tkinter import ttk, filedialog, messagebox
from .personsvm import PersonsVM
from .doctorvm import DoctorVM
from ..models.doctor import Doctor
from .validators import Validator as V

class DoctorsVM(PersonsVM):
    def __init__(self, data, updateIdCallback=lambda *_, **p:None, deleteCallback=lambda *_, **p: True):
        super().__init__(data, Doctor, DoctorVM, updateIdCallback, deleteCallback)
        self.specVar=StringVar()   
        self.cabinetVar=IntVar()
        self._tableName='Доктора'
    
    def isValidCabinet(self):
        return self.cabinetVar.get()>0

    def getAddArgs(self):
        d=super().getAddArgs()
        d['cabinet']=self.cabinetVar.get()
        d['speciality']=self.specVar.get()
        return d
    
    def updateRecord(self, record):
        super().updateRecord(record)
        record.cabinet.set(self.cabinetVar.get())
    
    def updateRecordId(self, record):
        super().updateRecordId(record)

    def getTableData(self):
        data=super().getTableData()
        for i in range(len(self.vmData)):
            data[i].append(self.vmData[i].cabinet)
            data[i].append(self.vmData[i].speciality)
        return data