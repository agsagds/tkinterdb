from .contactsvm import ContactsVM
from .contactvm import ContactVM
from ..models.contact import Contact


class EmContactsVM(ContactsVM):
    def __init__(self, data, updateIdCallback=lambda *_, **p:None, deleteCallback=lambda *_, **p: True):
        super().__init__(data, Contact, ContactVM, updateIdCallback, deleteCallback)
        self._tableName='Экстренные контакты'