from .record import Record
class Address(Record):
    def __init__(self, id, city, street, home, patientId):
        super().__init__(id)
        self._city=city
        self._street=street
        self._home=home
        self._patient=patientId
    
    def getCity(self):
        return self._city
        
    def getStreet(self):
        return self._street
    
    def getHome(self):
        return self._home
        
    def setCity(self, city):
        self._city=city
        
    def setStreet(self, street):
        self._street=street
    
    def setHome(self, home):
        self._home=home
    
    def getPatient(self):
        return self._patient
        
    def setPatient(self, patientId):
        self._patient=patientId