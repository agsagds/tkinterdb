from .person import Person
class Doctor(Person):
    def __init__(self, id, name, phone, sex, cabinet, speciality):
        super().__init__(id, name, phone, sex)
        self._speciality=speciality
        self._cabinet=cabinet


    def getSpeciality(self):
        return self._speciality
    
    def getCabinet(self):
        return self._cabinet
    
    def setCabinet(self, cabinet):
        self._cabinet=cabinet
        
    