from .record import Record


class Contact(Record):
    def __init__(self, id, name, phone):
        super().__init__(id)
        self._phone = phone
        self._name = name

    def getPhone(self):
        return self._phone

    def getName(self):
        return self._name

    def setPhone(self, phone):
        self._phone = phone

    def setName(self, name):
        self._name = name
