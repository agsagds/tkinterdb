from .person import Person
class Patient(Person):
    def __init__(self, id, name, phone, sex, idContact):
        super().__init__(id, name, phone, sex)
        self._emergencyContact=idContact
    
    def getEmergencyContact(self):
        return self._emergencyContact
    
    def setEmergencyContact(self, idContact):
        self._emergencyContact=idContact