from .record import Record
class Consultation(Record):
    def __init__(self, id, idPatient, idDoctor, reason):
        super().__init__(id)
        self._idPatient=idPatient
        self._idDoctor=idDoctor
        self._reason=reason

    def getPatient(self):
        return self._idPatient

    def setPatient(self, idPatient):
        self._idPatient=idPatient

    def getDoctor(self):
        return self._idDoctor

    def setDoctor(self, idDoctor):
        self._idDoctor=idDoctor

    def getReason(self):
        return self._reason
        
    def setReason(self, reason):
        self._reason=reason