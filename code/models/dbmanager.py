import pickle
class DBManager:
    
    @staticmethod
    def save(db, filepath):
        db._setNameFromFilepath(filepath)
        f=open(filepath,'wb')
        pickle.dump(db,f)
        f.close()
    
    @staticmethod
    def load(filepath):
        f=open(filepath,'rb')
        db=pickle.load(f)
        f.close()
        return db

    @staticmethod
    def getEmptyDB():
        return DBManager()
    
    def __init__(self, dbName='New DB'):
        self._dbName=dbName
        self._addresses=[]
        self._consultations=[]
        self._doctors=[]
        self._patients=[]
        self._emContacts=[]

    def getDoctorsTable(self):
        return self._doctors
        
    def getConsultTable(self):
        return self._consultations
        
    def getPatientsTable(self):
        return self._patients
        
    def getAddressTable(self):
        return self._addresses
        
    def getEmergencyTable(self):
        return self._emContacts
    
    def getName(self):
        return self._dbName

    def _setNameFromFilepath(self, filepath):
        a=filepath.split('/')[-1].split('\\')[-1].split('.')[:-1]
        self._dbName='.'.join(a)