from .contact import Contact
class Person(Contact):
    def __init__(self, id, name, phone, sex):
        super().__init__(id, name, phone)
        self._sex=sex

    def getSex(self):
        return self._sex
    