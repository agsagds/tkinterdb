class User:
    _loginsPath = 'logins.csv'

    @staticmethod
    def isUser(login, pswrd):
        # лезем в файл и проверяем что там есть запись
        with open(User._loginsPath, 'r', encoding='utf8') as f:
            for line in f:
                log, pas, grp = line.split()
                if log == login and pas == pswrd:
                    return True
        return False

    @staticmethod
    def _getGroup(login, pswrd):
        # todo: лезем в файл и берём группу для юзера
        with open(User._loginsPath, 'r', encoding='utf8') as f:
            for line in f:
                log, pas, grp = line.split()
                if log == login and pas == pswrd:
                    return grp
        return None

    def __init__(self, login, pswrd):
        self._login = login
        self._group = User._getGroup(login, pswrd)

    def getLogin(self):
        return self._login

    def getGroup(self):
        return self._group
