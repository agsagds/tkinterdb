from tkinter import *
from code.models.dbmanager import DBManager
from code.viewModels.vmfactory import VMFactory
from code.views.viewfactory import ViewFactory
from code.logger import Logger

if __name__=='__main__':
    root = Tk()
    mv=ViewFactory.getMainWindow(root, VMFactory(DBManager.getEmptyDB()))
    mv.pack()
    
    root.mainloop()
    Logger.logBeforeInit('Выход')